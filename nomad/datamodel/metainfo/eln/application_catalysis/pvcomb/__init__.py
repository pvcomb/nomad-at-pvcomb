#
# Copyright The NOMAD Authors.
#
# This file is part of NOMAD. See https://nomad-lab.eu for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# from nomad.units import ureg
from nomad.metainfo import (
    Package,
    Section)
from nomad.datamodel.data import EntryData

from nomad.datamodel.metainfo.eln.base_classes_hzb.heterogeneous_catalysis import (
    CatalyticSample, CatalyticSampleWithGrid)

from nomad.datamodel.metainfo.eln.base_classes_hzb.vapour_based_deposition import (
    PECVDeposition, PVDeposition)


m_package6 = Package(name='pvcomb')


# %% ####################### Entities

class pvcomb_CatalyticSample(CatalyticSample, EntryData):
    m_def = Section(
        a_eln=dict(
            hide=["users"],
            properties=dict(
                editable=dict(
                    exclude=["lab_id"]),
                order=[
                    "name",
                    "lab_id", "chemical_composition_or_formulas",
                    "sampe_area"])))


class pvcomb_CatalyticSampleWithGrid(CatalyticSampleWithGrid, EntryData):
    m_def = Section(
        a_eln=dict(
            hide=["users"],
            properties=dict(
                editable=dict(
                    exclude=["lab_id"]),
                order=[
                    "name",
                    "lab_id", "chemical_composition_or_formulas",
                    "sampe_area"])))


class pvcomb_PECVD(PECVDeposition, EntryData):
    m_def = Section(
        a_eln=dict(
            hide=[
                'lab_id',
                'users',
                'location',
                'end_time',
                "previous_process",
                "is_standard_process"],
            properties=dict(
                order=[
                    "name",
                    "recipe",
                    "logs",
                    "layer_type",
                    "layer_material_name",
                    "layer_material",
                    "batch",
                    "samples"])))


class pvcomb_PVD(PVDeposition, EntryData):
    m_def = Section(
        a_eln=dict(
            hide=[
                'lab_id',
                'users',
                'location',
                'end_time',
                "previous_process",
                "is_standard_process"],
            properties=dict(
                order=[
                    "name",
                    "layer_type",
                    "layer_material_name",
                    "layer_material",
                    "batch",
                    "samples"])))
