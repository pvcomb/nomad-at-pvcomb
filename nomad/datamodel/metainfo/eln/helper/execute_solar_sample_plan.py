#
# Copyright The NOMAD Authors.
#
# This file is part of NOMAD. See https://nomad-lab.eu for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

from nomad.datamodel.metainfo.eln import SampleID
from nomad.datamodel.metainfo.eln.base_classes_hzb import BatchID

from nomad.datamodel.metainfo.eln.helper.utilities import get_reference, create_archive, get_entry_id_from_file_name

from nomad.metainfo import MProxy


def add_next_md_line(key, item, indent=0):
    shift = '&nbsp;' * indent
    try:
        if isinstance(item, MProxy):
            print(item)
            print(item.name)
            return f"{shift}**{key.capitalize()}**: {item.name}  \n"
    except Exception as e:
        print(e)

    return f"{shift}**{key.capitalize()}**: {item}  \n"


def add_section_markdown(
        md,
        index_plan,
        index_batch,
        batch_process,
        process_batch):

    md += f"### {index_plan+1}.{index_batch+1} {batch_process.name.capitalize()}  \n"
    data_dict = batch_process.m_to_dict()
    md += f"**Batch Id**: {process_batch}  \n"

    for key, item in data_dict.items():
        if key in [
            "previous_process",
            "is_standard_process",
            "samples",
            "batch",
            "name",
            "datetime",
            "lab_id",
                "m_def"]:
            continue

        if isinstance(item, dict):
            md += f"**{key.capitalize()}**:  \n"
            for key2 in item.keys():
                subsection = getattr(batch_process, key)
                md += add_next_md_line(key2, getattr(subsection, key2), 4)
        else:
            md += add_next_md_line(key, getattr(batch_process, key))

    return md


def execute_solar_sample_plan(plan_obj, archive, sample_cls, batch_cls):

    # standard process integration
    if plan_obj.load_standard_processes:
        plan_obj.load_standard_processes = False

        number_of_batches = plan_obj.number_of_substrates // plan_obj.substrates_per_subbatch
        for i, process in enumerate(plan_obj.standard_plan.processes):
            if not plan_obj.plan[i].vary_parameters:
                plan_obj.plan[i].batch_processes = [process]
                continue
            plan_obj.plan[i].batch_processes = [
                process] * number_of_batches

    # process, sample and batch creation
    if plan_obj.create_samples_and_processes and plan_obj.batch_id and plan_obj.batch_id.sample_id:
        plan_obj.create_samples_and_processes = False
        batch_id = BatchID(**plan_obj.batch_id.m_to_dict())
        sample_short_name = plan_obj.batch_id.sample_short_name
        sample_id = SampleID(**batch_id.m_to_dict())
        # create samples and batches
        sample_refs = []
        subs = plan_obj.standard_plan.substrate
        architecture = plan_obj.standard_plan.architecture
        for idx1 in range(
                plan_obj.number_of_substrates // plan_obj.substrates_per_subbatch):
            sample_refs_subbatch = []
            for idx2 in range(plan_obj.substrates_per_subbatch):

                sample_id.sample_short_name = f"{sample_short_name}_{idx1}_{idx2}"
                # For each sample number, create instance of sample.
                file_name = f'{plan_obj.batch_id.sample_id}_{idx1}_{idx2}.archive.json'
                sample = sample_cls(
                    name=f'{plan_obj.name} {plan_obj.batch_id.sample_id}_{idx1}_{idx2}',
                    sample_id=sample_id,
                    datetime=plan_obj.datetime,
                    substrate=subs,
                    architecture=architecture,
                    description=plan_obj.description if plan_obj.description else None)
                create_archive(sample, archive, file_name)
                entry_id = get_entry_id_from_file_name(file_name, archive)
                # print(get_reference(archive.metadata.upload_id, entry_id))
                sample_refs_subbatch.append(get_reference(
                    archive.metadata.upload_id, entry_id))
            sample_refs.append(sample_refs_subbatch)
            file_name = f'{plan_obj.batch_id.sample_id}_{idx1}.archive.json'
            batch_id.sample_short_name = f"{sample_short_name}_{idx1}"
            subbatch = batch_cls(
                name=f'{plan_obj.name} {plan_obj.batch_id.sample_id}_{idx1}',
                datetime=plan_obj.datetime,
                batch_id=batch_id,
                samples=sample_refs_subbatch,
                description=plan_obj.description if plan_obj.description else None)
            create_archive(subbatch, archive, file_name)

        file_name = f'{plan_obj.batch_id.sample_id}.archive.json'
        batch_id.sample_short_name = f"{sample_short_name}"
        batch = batch_cls(
            name=f'{plan_obj.name} {plan_obj.batch_id.sample_id}',
            batch_id=batch_id,
            datetime=plan_obj.datetime,
            samples=[
                item for sublist in sample_refs for item in sublist],
            description=plan_obj.description if plan_obj.description else None)
        create_archive(batch, archive, file_name)

        # create processes
        previous_processes = []
        md = f"# Batch plan of batch {batch_id.sample_id}\n\n"
        for idx2, plan in enumerate(plan_obj.plan):
            previous_processes_tmp = previous_processes.copy()
            previous_processes = []
            for idx1, batch_process in enumerate(plan.batch_processes):
                file_name_base = f'{plan_obj.batch_id.sample_id}_{idx1}' if \
                    plan.vary_parameters else \
                    f'{plan_obj.batch_id.sample_id}'
                file_name = f"{file_name_base}.archive.json"
                entry_id = get_entry_id_from_file_name(file_name, archive)

                batch_process.name = batch_process.name.replace(
                    "Standard", "").replace("-", "").strip()

                batch_process.batch = get_reference(
                    archive.metadata.upload_id, entry_id)

                if "function" in batch_process:
                    name = f'{batch_process.function}'
                elif "name" in batch_process:
                    name = f'{batch_process.name}'
                else:
                    name = f'{batch_process.method}'

                if file_name_base not in name:
                    name = f'{name} {file_name_base}'

                batch_process.previous_process = previous_processes_tmp
                # file_name = f'{name.replace(" ","_")}_{file_name_base}_{randStr()}.archive.json'
                file_name = f'{idx2+1}_{name.replace(" ","_")}.archive.json'
                entry_id = get_entry_id_from_file_name(file_name, archive)
                previous_processes.append(get_reference(
                    archive.metadata.upload_id, entry_id))
                batch_process.is_standard_process = False

                if plan_obj.description:
                    batch_process.description = plan_obj.description if not batch_process.description \
                        else f'{batch_process.description}\n\n{plan_obj.description}'

                batch_process.description
                # todo add one second
                batch_process.datetime = plan_obj.datetime if plan_obj.datetime else ''
                create_archive(batch_process, archive, file_name)
                md = add_section_markdown(
                    md, idx2, idx1, batch_process, file_name_base)

        from weasyprint import HTML
        from markdown2 import Markdown
        markdowner = Markdown()
        md = md.replace("_", "\\_")
        html = markdowner.convert(md)
        output = f"batch_plan_{batch_id.sample_id}.pdf"
        with archive.m_context.raw_file(output, 'w') as outfile:
            print(outfile.name)
            HTML(string=html).write_pdf(outfile.name)
        plan_obj.batch_plan_pdf = output
