# Copyright The NOMAD Authors.
#
# This file is part of NOMAD. See https://nomad-lab.eu for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

from nomad.metainfo import (
    Package,
    Section)

from nomad.datamodel.data import EntryData

from ..TEM_EDX_detector import TEM_EDX
from ..TEM_Gatam_US1000_detector import TEM_Gatam_US1000
from ..TEM_Lambda_750k_detector import TEM_lambda750k
from ..TEM_HAADE_detector import TEM_HAADE
from ..SEM_Zeiss_detector import SEM_Microscope_Merlin
from ..TEM_Session import TEM_Session
from ..microscope import TEMMicroscopeConfiguration

from nomad.datamodel.metainfo.eln import Sample


m_package3 = Package(name='Wannsee_EM')


class Wannsee_Sample(Sample, EntryData):
    m_def = Section(
        a_eln=dict(hide=['lab_id', 'author', 'user']))


class Wannsee_EM_M013_TEM_Imaging_lambda750(TEM_lambda750k, EntryData):
    m_def = Section(
        a_eln=dict(hide=['lab_id', 'author', 'user']))

# for backwards compatibility


class M013_TEM_Imaging_lambda750k(Wannsee_EM_M013_TEM_Imaging_lambda750):
    m_def = Section(
        a_eln=dict(hide=['lab_id', 'author', 'user']))


class Wannsee_EM_M013_TEM_Imaging_GatamUS1000(TEM_Gatam_US1000, EntryData):
    m_def = Section(
        a_eln=dict(hide=['lab_id', 'author', 'user']))


class Wannsee_EM_M013_TEM_Scanning_HAADE(TEM_HAADE, EntryData):
    m_def = Section(
        a_eln=dict(hide=['lab_id', 'author', 'user']))


class Wannsee_EM_M013_TEM_Scanning_EDX(TEM_EDX, EntryData):
    m_def = Section(
        a_eln=dict(hide=['lab_id', 'author', 'user']))


class Wannsee_EM_M013_TEM_Session(TEM_Session, EntryData):
    m_def = Section(
        a_eln=dict(hide=['lab_id', 'author', 'user']))

# for backwards compatibility


class M013_TEM_Imaging_lambda750k_Session(Wannsee_EM_M013_TEM_Session):
    m_def = Section(
        a_eln=dict(hide=['lab_id', 'author', 'user']))


class Wannsee_EM_M013_TEM_Configuration(TEMMicroscopeConfiguration, EntryData):
    m_def = Section(
        a_eln=dict(hide=['lab_id', 'author', 'user']))


class Wannsee_EM_M001_SEM_Merlin(SEM_Microscope_Merlin, EntryData):
    m_def = Section(
        a_eln=dict(hide=['lab_id', 'author', 'user']))
