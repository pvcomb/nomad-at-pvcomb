

# from nomad.units import ureg
from nomad.metainfo import (
    Package,
    Quantity,
    SubSection,
    Section)

from nomad.datamodel.data import EntryData
from nomad.datamodel.results import Results, Properties, Material, ELN


from nomad.datamodel.metainfo.eln.base_classes_hzb import (
    ProcessOnSample, MeasurementOnSample, LayerDeposition, Batch
)

from nomad.datamodel.metainfo.eln.base_classes_hzb.chemical import (
    Solid, Gas, Powder, Solvent
)

from nomad.datamodel.metainfo.eln.base_classes_hzb.solution import Solution
from nomad.datamodel.metainfo.eln.base_classes_hzb.experimental_plan import ExperimentalPlan

from nomad.datamodel.metainfo.eln.base_classes_hzb.wet_chemical_deposition import (
    SpinCoating, SpinCoatingRecipe, SlotDieCoating)

from nomad.datamodel.metainfo.eln.base_classes_hzb.vapour_based_deposition import (
    Evaporations, Sputtering)

from nomad.datamodel.metainfo.eln.base_classes_hzb.material_processes_misc import (
    Cleaning, SolutionCleaning, PlasmaCleaning,
    ThermalAnnealing,
    SpinCoatingAntiSolvent
)

from nomad.datamodel.metainfo.eln.base_classes_hzb.solar_energy import (
    StandardSampleSolarCell,
    JVMeasurement,
    EQEMeasurement,
    Substrate,
    SolcarCellSample, MPPTracking
)

m_package4 = Package(name='HySprint')

# %% ####################### Entities


class peroTF_ExperimentalPlan(ExperimentalPlan, EntryData):
    m_def = Section(
        a_eln=dict(hide=['users'],
                   properties=dict(
            order=[
                "name",
                "standard_plan",
                "load_standard_processes",
                "create_samples_and_processes",
                "number_of_substrates",
                "substrates_per_subbatch",
                "lab_id"
            ])),
        a_template=dict(institute="KIT_perotf"))

    def normalize(self, archive, logger):
        super(peroTF_ExperimentalPlan, self).normalize(archive, logger)
        if not (self.standard_plan and self.number_of_substrates > 0
                and self.number_of_substrates % self.substrates_per_subbatch == 0
                and self.plan and self.standard_plan.processes):
            return

        from nomad.datamodel.metainfo.eln.helper.execute_solar_sample_plan import execute_solar_sample_plan
        execute_solar_sample_plan(self, archive, peroTF_Sample, peroTF_Batch)

        # actual normalization!!
        archive.results = Results()
        archive.results.properties = Properties()
        archive.results.material = Material()
        archive.results.eln = ELN()
        archive.results.eln.sections = ["peroTF_ExperimentalPlan"]


class peroTF_StandardSample(StandardSampleSolarCell, EntryData):
    m_def = Section(
        a_eln=dict(hide=['lab_id', 'users']))


class peroTF_SpinCoating_Recipe(SpinCoatingRecipe, EntryData):
    m_def = Section(
        a_eln=dict(hide=['lab_id', 'users']))


class peroTF_Solvent(Solvent, EntryData):
    m_def = Section(
        a_eln=dict(hide=['lab_id', 'users']))


class peroTF_Powder(Powder, EntryData):
    m_def = Section(
        a_eln=dict(hide=['lab_id', 'users']))


class peroTF_Solid(Solid, EntryData):
    m_def = Section(
        a_eln=dict(hide=['lab_id', 'users']))


class peroTF_Gas(Gas, EntryData):
    m_def = Section(
        a_eln=dict(hide=['lab_id', 'users']))


class peroTF_Substrate(Substrate, EntryData):
    m_def = Section(
        a_eln=dict(hide=['lab_id', 'users'],
                   properties=dict(
            order=[
                "name",
                "substrate",
                "conducting_material",
                "solar_cell_area",
                "pixel_area",
                "number_of_pixels"])))


class peroTF_Solution(Solution, EntryData):
    m_def = Section(
        a_eln=dict(hide=['lab_id', 'users', 'chemical_formula'],
                   properties=dict(
            order=[
                "name",
                "method",
                "temperature",
                "time",
                "speed",
                "solvent_ratio"])),
        a_template=dict(temperature=45, time=15, method='Shaker'))


class peroTF_Sample(SolcarCellSample, EntryData):
    m_def = Section(
        a_eln=dict(hide=['users'], properties=dict(
            order=["name", "substrate", "architecture"])),
        a_template=dict(institute="KIT_taskforce"),
        label_quantity='sample_id'
    )


class peroTF_Batch(Batch, EntryData):
    m_def = Section(
        a_eln=dict(hide=['lab_id', 'users'],
                   properties=dict(
            order=[
                "name",
                "samples",
                "export_batch_ids",
                "csv_export_file"])))


# %% ####################### Cleaning


class peroTF_CR_Wetbench_Cleaning(Cleaning, EntryData):
    m_def = Section(
        a_eln=dict(
            hide=[
                'lab_id', 'users', 'location', 'end_time'],
            properties=dict(
                order=[
                    "name",
                    "is_standard_process",
                    "previous_process",
                    "batch",
                    "samples"])))

    cleaning = SubSection(
        section_def=SolutionCleaning, repeats=True)


# class peroTF_114_HyFlowBox_Cleaning_UV(Cleaning, EntryData):
#     m_def = Section(
#         a_eln=dict(
#             hide=[
#                 'lab_id', 'users', 'location', 'end_time']))

#     cleaning = SubSection(
#         section_def=UVCleaning, repeats=True)


class peroTF_CR_Plasma_Cleaning(Cleaning, EntryData):
    m_def = Section(
        a_eln=dict(
            hide=[
                'lab_id', 'users', 'location', 'end_time'],
            properties=dict(
                order=[
                    "name",
                    "is_standard_process",
                    "previous_process",
                    "batch",
                    "samples"])))

    cleaning = SubSection(
        section_def=PlasmaCleaning, repeats=True)


# %% ##################### Layer Deposition


# class peroTF_114_HTFumeHood_SprayPyrolysis(SprayPyrolysis, EntryData):
#     m_def = Section(
#         a_eln=dict(
#             hide=[
#                 'lab_id', 'layer', 'user', 'author']))


# %% ### Spin Coating

class peroTF_CR_SpinBox_SpinCoating(SpinCoating, EntryData):
    m_def = Section(
        a_eln=dict(
            hide=[
                'lab_id', 'users', 'location', 'end_time'],
            properties=dict(
                order=[
                    "name",
                    "is_standard_process",
                    "layer_type",
                    "layer_material_name",
                    "layer_material",
                    "recipe",
                    "previous_process",
                    "batch",
                    "samples"])), a_template=dict(
            layer_type="Absorber Layer",
        ))

    anti_solvent = SubSection(
        section_def=SpinCoatingAntiSolvent, repeats=True)


class peroTF_CR_ChemistryBox_SpinCoating(SpinCoating, EntryData):
    m_def = Section(
        a_eln=dict(
            hide=[
                'lab_id', 'users', 'location', 'end_time'],
            properties=dict(
                order=[
                    "name",
                    "is_standard_process",
                    "layer_type",
                    "layer_material_name",
                    "layer_material",
                    "recipe",
                    "previous_process",
                    "batch",
                    "samples"])), a_template=dict(
            layer_type="Absorber Layer",
        ))

    anti_solvent = SubSection(
        section_def=SpinCoatingAntiSolvent, repeats=True)


class peroTF_CR_MixBox_SpinCoating(SpinCoating, EntryData):
    m_def = Section(
        a_eln=dict(
            hide=[
                'lab_id', 'users', 'location', 'end_time'],
            properties=dict(
                order=[
                    "name",
                    "is_standard_process",
                    "layer_type",
                    "layer_material_name",
                    "layer_material",
                    "recipe",
                    "previous_process",
                    "batch",
                    "samples"])), a_template=dict(
            layer_type="Hole Transport Layer",
        ))


class peroTF_CR_OmegaBox_SpinCoating(SpinCoating, EntryData):
    m_def = Section(
        a_eln=dict(
            hide=[
                'lab_id', 'users', 'location', 'end_time'],
            properties=dict(
                order=[
                    "name",
                    "is_standard_process",
                    "layer_type",
                    "layer_material_name",
                    "layer_material",
                    "recipe",
                    "previous_process",
                    "batch",
                    "samples"])), a_template=dict(
            layer_type="Absorber Layer",
        ))

    anti_solvent = SubSection(
        section_def=SpinCoatingAntiSolvent, repeats=True)


class peroTF_TFL_AlphaBox_SpinCoating(SpinCoating, EntryData):
    m_def = Section(
        a_eln=dict(
            hide=[
                'lab_id', 'users', 'location', 'end_time'],
            properties=dict(
                order=[
                    "name",
                    "is_standard_process",
                    "layer_type",
                    "layer_material_name",
                    "layer_material",
                    "recipe",
                    "previous_process",
                    "batch",
                    "samples"])), a_template=dict(
            layer_type="Absorber Layer",
        ))

    anti_solvent = SubSection(
        section_def=SpinCoatingAntiSolvent, repeats=True)


class peroTF_CR_BetaBox_SpinCoating(SpinCoating, EntryData):
    m_def = Section(
        a_eln=dict(
            hide=[
                'lab_id', 'users', 'location', 'end_time'],
            properties=dict(
                order=[
                    "name",
                    "is_standard_process",
                    "layer_type",
                    "layer_material_name",
                    "layer_material",
                    "recipe",
                    "previous_process",
                    "batch",
                    "samples"])), a_template=dict(
            layer_type="Absorber Layer",
        ))

    anti_solvent = SubSection(
        section_def=SpinCoatingAntiSolvent, repeats=True)


# %% ### Slot Die Coating


class peroTF_UP_SlotDieBox_SlotDieCoating(SlotDieCoating, EntryData):
    m_def = Section(
        a_eln=dict(
            hide=[
                'lab_id',
                'users',
                'location', 'end_time'],
            properties=dict(
                order=[
                    "name",
                    "is_standard_process",
                    "layer_type",
                    "layer_material_name",
                    "layer_material",
                    "previous_process",
                    "batch",
                    "samples"])),
        a_template=dict(
            layer_type="Absorber Layer"))


# %% ### Annealing

class peroTF_CR_SpinBox_ThermalAnnealing(ThermalAnnealing, EntryData):
    m_def = Section(
        a_eln=dict(
            hide=[
                'lab_id', 'users', 'location', 'end_time', 'humidity'],
            properties=dict(
                order=[
                    "name",
                    "is_standard_process",
                    "temperature",
                    "time",
                    "function",
                    "previous_process",
                    "batch",
                    "samples"])))


class peroTF_CR_MixBox_ThermalAnnealing(ThermalAnnealing, EntryData):
    m_def = Section(
        a_eln=dict(
            hide=[
                'lab_id', 'users', 'location', 'end_time', 'humidity'],
            properties=dict(
                order=[
                    "name",
                    "is_standard_process",
                    "temperature",
                    "time",
                    "function",
                    "previous_process",
                    "batch",
                    "samples"])))


class peroTF_CR_ChemistryBox_ThermalAnnealing(ThermalAnnealing, EntryData):
    m_def = Section(
        a_eln=dict(
            hide=[
                'lab_id', 'users', 'location', 'end_time', 'humidity'],
            properties=dict(
                order=[
                    "name",
                    "is_standard_process",
                    "temperature",
                    "time",
                    "function",
                    "previous_process",
                    "batch",
                    "samples"])))


class peroTF_CR_OmegaBox_ThermalAnnealing(ThermalAnnealing, EntryData):
    m_def = Section(
        a_eln=dict(
            hide=[
                'lab_id', 'users', 'location', 'end_time', 'humidity'],
            properties=dict(
                order=[
                    "name",
                    "is_standard_process",
                    "temperature",
                    "time",
                    "function",
                    "previous_process",
                    "batch",
                    "samples"])))


class peroTF_CR_Wetbench_ThermalAnnealing(ThermalAnnealing, EntryData):
    m_def = Section(
        a_eln=dict(
            hide=[
                'lab_id', 'users', 'location', 'end_time', 'humidity'],
            properties=dict(
                order=[
                    "name",
                    "is_standard_process",
                    "temperature",
                    "time",
                    "function",
                    "previous_process",
                    "batch",
                    "samples"])))


class peroTF_CR_Fumehood_ThermalAnnealing(ThermalAnnealing, EntryData):
    m_def = Section(
        a_eln=dict(
            hide=[
                'lab_id', 'users', 'location', 'end_time', 'humidity'],
            properties=dict(
                order=[
                    "name",
                    "is_standard_process",
                    "temperature",
                    "time",
                    "function",
                    "previous_process",
                    "batch",
                    "samples"])))


class peroTF_TFL_AlphaBox_ThermalAnnealing(ThermalAnnealing, EntryData):
    m_def = Section(
        a_eln=dict(
            hide=[
                'lab_id', 'users', 'location', 'end_time', 'humidity'],
            properties=dict(
                order=[
                    "name",
                    "is_standard_process",
                    "temperature",
                    "time",
                    "function",
                    "previous_process",
                    "batch",
                    "samples"])))


class peroTF_TFL_BetaBox_ThermalAnnealing(ThermalAnnealing, EntryData):
    m_def = Section(
        a_eln=dict(
            hide=[
                'lab_id', 'users', 'location', 'end_time', 'humidity'],
            properties=dict(
                order=[
                    "name",
                    "is_standard_process",
                    "temperature",
                    "time",
                    "function",
                    "previous_process",
                    "batch",
                    "samples"])))


class peroTF_TFL_Fumehood_ThermalAnnealing(ThermalAnnealing, EntryData):
    m_def = Section(
        a_eln=dict(
            hide=[
                'lab_id', 'users', 'location', 'end_time', 'humidity'],
            properties=dict(
                order=[
                    "name",
                    "is_standard_process",
                    "temperature",
                    "time",
                    "function",
                    "previous_process",
                    "batch",
                    "samples"])))


# %% ### Evaporation


class peroTF_TFL_SputterSystem_Sputtering(Sputtering, EntryData):
    m_def = Section(
        a_eln=dict(
            hide=[
                'lab_id', 'users', 'location', 'end_time'],
            properties=dict(
                order=[
                    "name",
                    "is_standard_process",
                    "layer_type",
                    "layer_material_name",
                    "layer_material",
                    "previous_process",
                    "batch",
                    "samples"])))


class peroTF_TFL_Ebeam_Evaporation(Evaporations, EntryData):
    m_def = Section(
        a_eln=dict(
            hide=[
                'lab_id', 'users', 'location', 'end_time'],
            properties=dict(
                order=[
                    "name",
                    "is_standard_process",
                    "layer_type",
                    "layer_material_name",
                    "layer_material",
                    "previous_process",
                    "batch",
                    "samples"])))


class peroTF_TFL_BellJar_Evaporation(Evaporations, EntryData):
    m_def = Section(
        a_eln=dict(
            hide=[
                'lab_id', 'users', 'location', 'end_time'],
            properties=dict(
                order=[
                    "name",
                    "is_standard_process",
                    "layer_type",
                    "layer_material_name",
                    "layer_material",
                    "previous_process",
                    "batch",
                    "samples"])))


class peroTF_UP_PEROvap_Evaporation(Evaporations, EntryData):
    m_def = Section(
        a_eln=dict(
            hide=[
                'lab_id', 'users', 'location', 'end_time'],
            properties=dict(
                order=[
                    "name",
                    "is_standard_process",
                    "layer_type",
                    "layer_material_name",
                    "layer_material",
                    "previous_process",
                    "batch",
                    "samples"])))


class peroTF_UP_OPTIvap_Evaporation(Evaporations, EntryData):
    m_def = Section(
        a_eln=dict(
            hide=[
                'lab_id', 'users', 'location', 'end_time'],
            properties=dict(
                order=[
                    "name",
                    "is_standard_process",
                    "layer_type",
                    "layer_material_name",
                    "layer_material",
                    "previous_process",
                    "batch",
                    "samples"])))


# %% ## Storage


# class peroTF_108_HyDryAir_Storage(Storage, EntryData):
#     m_def = Section(
#         a_eln=dict(
#             hide=[
#                 'lab_id', 'users', 'location', 'end_time']))


# %%####################################### Measurements


class peroTF_CR_SolSimBox_JVmeasurement(JVMeasurement, EntryData):
    m_def = Section(
        a_eln=dict(
            hide=[
                'lab_id',
                'users',
                'location',
                'end_time',
                'certified_values',
                'certification_institute'],
            properties=dict(
                order=[
                    "name",
                    "data_file",
                    "active_area",
                    "intensity",
                    "integration_time",
                    "settling_time",
                    "averaging",
                    "compliance",
                    "samples"])),
        a_plot=[
            {
                'x': 'jv_curve/:/voltage',
                'y': 'jv_curve/:/current_density',
                'layout': {
                    "showlegend": True,
                    'yaxis': {
                        "fixedrange": False},
                    'xaxis': {
                        "fixedrange": False}},
            }])


class peroTF_CR_SolSimBox_MPPTracking(MPPTracking, EntryData):
    m_def = Section(
        a_eln=dict(
            hide=[
                'lab_id',
                'users',
                'location',
                'end_time', ],
            properties=dict(
                order=[
                    "name",
                    "data_file",
                    "samples"])),
        a_plot=[
            {
                'x': 'time',
                'y': ['efficiency', 'voltage'],
                'layout': {
                    "showlegend": True,
                    'yaxis': {
                        "fixedrange": False},
                    'xaxis': {
                        "fixedrange": False}},
            }])


class peroTF_TFL_GammaBox_JVmeasurement(JVMeasurement, EntryData):
    m_def = Section(
        a_eln=dict(
            hide=[
                'lab_id',
                'users',
                'location',
                'end_time',
                'certified_values',
                'certification_institute'],
            properties=dict(
                order=[
                    "name",
                    "data_file",
                    "active_area",
                    "intensity",
                    "integration_time",
                    "settling_time",
                    "averaging",
                    "compliance",
                    "samples"])),
        a_plot=[
            {
                'x': 'jv_curve/:/voltage',
                'y': 'jv_curve/:/current_density',
                'layout': {
                    "showlegend": True,
                    'yaxis': {
                        "fixedrange": False},
                    'xaxis': {
                        "fixedrange": False}},
            }])


class peroTF_TFL_GammaBox_EQEmeasurement(EQEMeasurement, EntryData):
    m_def = Section(
        a_eln=dict(
            hide=[
                'lab_id', 'users', 'location', 'end_time'],
            properties=dict(
                order=[
                    "name",
                    "data_file",
                    "samples"])))


# class peroTF_108_HyPrint_PLmeasurement(PLMeasurement, EntryData):
#     m_def = Section(
#         a_eln=dict(
#             hide=[
#                 'lab_id', 'users', 'location', 'end_time']))


# class peroTF_1xx_nobox_UVvismeasurement(UVvisMeasurement, EntryData):
#     m_def = Section(
#         a_eln=dict(
#             hide=[
#                 'lab_id', 'users', 'location', 'end_time']))

# %%####################################### Generic Entries


class peroTF_Process(ProcessOnSample, EntryData):
    m_def = Section(
        a_eln=dict(
            hide=[
                'lab_id', 'users', 'location', 'end_time'],
            properties=dict(
                order=[
                    "name",
                    "is_standard_process",
                    "data_file",
                    "batch",
                    "samples"])))

    data_file = Quantity(
        type=str,
        shape=['*'],
        a_eln=dict(component='FileEditQuantity'),
        a_browser=dict(adaptor='RawFileAdaptor'))


class peroTF_Deposition(LayerDeposition, EntryData):
    m_def = Section(
        a_eln=dict(
            hide=[
                'lab_id', 'users', 'location', 'end_time'],
            properties=dict(
                order=[
                    "name",
                    "is_standard_process",
                    "layer_type",
                    "layer_material_name",
                    "layer_material",
                    "data_file",
                    "previous_process",
                    "batch",
                    "samples"])))

    data_file = Quantity(
        type=str,
        shape=['*'],
        a_eln=dict(component='FileEditQuantity'),
        a_browser=dict(adaptor='RawFileAdaptor'))


class peroTF_Measurement(MeasurementOnSample, EntryData):
    m_def = Section(
        a_eln=dict(
            hide=[
                'lab_id', 'users', 'location', 'end_time'],
            properties=dict(
                order=[
                    "name",
                    "data_file",
                    "samples"])))

    data_file = Quantity(
        type=str,
        shape=['*'],
        a_eln=dict(component='FileEditQuantity'),
        a_browser=dict(adaptor='RawFileAdaptor'))
